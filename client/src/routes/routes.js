import Home from 'containers/Home/Home';
import About from 'containers/Home/About/About';
import Connect from 'containers/Home/Connect/Connect';

/** 
 * Cart Routes
 */
import CartCheckout from 'containers/Cart/Checkout/Checkout'
import CartComplete from 'containers/Cart/Complete/Complete'
import CartPayment from 'containers/Cart/Payment/Payment';
import CartPaxInfo from 'containers/Cart/PaxInfo/PaxInfo';

/** 
 * Product Routes
 */
import ProductDetails from 'containers/Product/Details/Details';
import ProductDepartures from 'containers/Product/Departures/Departures';
import ProductOptions from 'containers/Product/Options/Options';
import ProductPickups from 'containers/Product/Pickups/Pickups';

/** 
 * Reservation Routes
 */
import Reservation from 'containers/Reservation/Reservation';
import ReservationDetails from 'containers/Reservation/Details/Details';

/** 
 * Search Routes
 */
import SearchDestination from 'containers/Search/Destination/Destination';
import SearchOperator from 'containers/Search/Operator/Operator';

/** 
 * User Routes
 */
import UserAuth from 'containers/User/Auth/Auth';
import UserDetails from 'containers/User/Details/Details';
import UserList from 'containers/User/List/List';
import UserLogout from 'containers/User/Logout/Logout';

export const routes = [
  {
    component: Home,
    exact: true,
    id: 'public-home-01',
    path: '/'
  }, {
    component: About,
    exact: true,
    id: 'public-about-01',
    path: '/about'
  }, {
    component: Connect,
    exact: true,
    id: 'public-connect-01',
    path: '/connect'
  },
  /** 
   * Cart Routes
   */
  {
    component: CartCheckout,
    exact: true,
    id: 'private-checkout-01',
    path: '/be/cart/:cartId/tour/:parentId/checkout'
  }, {
    component: CartComplete,
    exact: true,
    id: 'private-complete-01',
    path: '/be/cart/:cartId/tour/:parentId/complete'
  }, {
    component: CartPayment,
    exact: true,
    id: 'private-payment-01',
    path: '/be/cart/:cartId/tour/:parentId/pay'
  }, {
    component: CartPaxInfo,
    exact: true,
    id: 'private-paxinfo-01',
    path: '/be/cart/tour/:productId/:productDate/:passengers'
  }, {
    component: CartPaxInfo,
    exact: true,
    id: 'private-paxinfo-02',
    path: '/be/cart/tour/:parentId/flavour/:productId/:productDate/:passengers'
  }, {
    component: CartPaxInfo,
    exact: true,
    id: 'private-paxinfo-03',
    path: '/be/cart/tour/:productId/:productDate/:passengers/pickups/:pickupList'
  }, {
    component: CartPaxInfo,
    exact: true,
    id: 'private-paxinfo-04',
    path: '/be/cart/tour/:parentId/flavour/:productId/:productDate/:passengers/pickups/:pickupList'
  },
  /** 
   * Product Routes
   */
  {
    component: ProductDetails,
    exact: true,
    id: 'private-details-01',
    path: '/be/product/details/:productType/:productId/:startDate/:passengers'
  }, {
    component: ProductDepartures,
    exact: true,
    id: 'private-departures-01',
    path: '/be/product/departures/:productType/:productId/:startDate/:passengers'
  }, {
    component: ProductOptions,
    exact: true,
    id: 'private-options-01',
    path: '/be/product/options/tour/:productId/:startDate/:passengers'
  }, {
    component: ProductOptions,
    exact: true,
    id: 'private-options-02',
    path: '/be/product/options/tour/:parentId/flavour/:productId/:startDate/:passengers'
  }, {
    component: ProductPickups,
    exact: true,
    id: 'private-pickups-01',
    path: '/be/product/pickups/tour/:productId/:startDate/:passengers'
  }, {
    component: ProductPickups,
    exact: true,
    id: 'private-pickups-02',
    path: '/be/product/pickups/tour/:parentId/flavour/:productId/:startDate/:passengers'
  },
  /** 
   * Reservation Routes
   */
  {
    component: Reservation,
    exact: true,
    id: 'private-reservation-01',
    path: '/be/reservations'
  }, {
    component: ReservationDetails,
    exact: true,
    id: 'private-reservation-details-01',
    path: '/be/reservations/:reservationId'
  },
  /** 
   * Search Routes
   */
  {
    component: SearchDestination,
    exact: true,
    id: 'private-destination-01',
    path: '/be/destination'
  }, {
    component: SearchOperator,
    exact: true,
    id: 'private-operator-01',
    path: '/be/operator'
  },
  /** 
   * User Routes
   */
  {
    component: UserAuth,
    exact: true,
    id: 'private-user-auth-01',
    path: '/login'
  }, {
    component: UserDetails,
    exact: true,
    id: 'private-user-details-01',
    path: '/be/user/:userId'
  }, {
    component: UserList,
    exact: true,
    id: 'private-user-list-01',
    path: '/be/users/all'
  }, {
    component: UserLogout,
    exact: true,
    id: 'public-logout-01',
    path: '/logout'
  }
]