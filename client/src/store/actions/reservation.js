import axios from 'store/actions/axios-config';

import {
  MESSAGE_CLEAR,
  MESSAGE_ERROR,
  MESSAGE_GET,
  RESERVATION_GET_START,
  RESERVATION_GET_COMPLETE,
  RESERVATION_SEARCH_START,
  RESERVATION_SEARCH_COMPLETE,
  RESERVATION_TICKET_START,
  RESERVATION_TICKET_COMPLETE
} from 'store/actions/types';
import { download, errorHandler } from 'store/helpers';

/**
 * Reservation actions
 */

/**
 * @name                getReservation
 * @description         Get the details for a reservation by its id.
 * @param               reservationId
 * @route               The Livn API route for this is https://*.livngds.com/livngds/api/reservations/{reservationId}
 */
export function getReservation(reservationId) {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: RESERVATION_GET_START })
    axios.get('/reservations/' + reservationId)
      .then(response => dispatch({ type: RESERVATION_GET_COMPLETE, payload: response.data }))
      .catch(error => errorHandler(dispatch, error, MESSAGE_ERROR))
  }
}

/**
 * @name                getReservationSearch
 * @description         Retrieves all reservations belonging to your API user's outlet group, by pax name/email,
 *                      ignoring case and including partial matches.
 * @param               values: { searchValue }
 * @route               The Livn API route for this is https://*.livngds.com/livngds/api/reservations/search
 */
export function getReservationSearch(values) {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: RESERVATION_SEARCH_START })
    const { searchValue, startDate, endDate } = values;
    let params = { searchValue };
    if (startDate && endDate) {
      params.startDate = startDate;
      params.endDate = endDate;
    }
    axios.get('/reservations/search', { params })
      .then(response => {
        const { data } = response;
        if (data.length < 1) {
          errorHandler(dispatch, 'There were no reservations found with that passenger last name or email and date range. Please try again.', MESSAGE_GET)
        }
        dispatch({ type: RESERVATION_SEARCH_COMPLETE, payload: data })
      })
      .catch(error => errorHandler(dispatch, error, MESSAGE_ERROR))
  }
}

/**
 * @name                getReservationTickets
 * @description         Retrieves a collated PDF with all tickets for the specified reservation.
 * @param               reservationId
 * @route               The Livn API route for this is https://*.livngds.com/livngds/api/reservations/{reservationId}/tickets
 */
export function getReservationTickets(reservationId) {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: RESERVATION_TICKET_START })
    axios.get('/reservations/' + reservationId + '/tickets', {
      headers: { 'Accept': 'application/pdf' },
      responseType: 'arraybuffer'
    })
      .then(response => {
        dispatch({ type: RESERVATION_TICKET_COMPLETE })
        dispatch(download(response.data, `Tickets-${reservationId}.pdf`));
      })
      .catch(error => errorHandler(dispatch, error, MESSAGE_ERROR))
  }
}