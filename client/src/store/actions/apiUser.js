import axios from 'store/actions/axios-config';
import {
  MESSAGE_CLEAR,
  MESSAGE_ERROR,
  API_USER_GET_START,
  API_USER_GET_COMPLETE
} from 'store/actions/types';
import { errorHandler } from 'store/helpers';

/**
 * API User actions
 */

/**
 * @name                getAPIUser
 * @description         Retrieves detailed information about the API user, it's parent outlet (travel agency/shop),
 *                      outletGroup (agency consortium) and distributor (wholesaler).
 * @route               The Livn API route for this is https://*.livngds.com/livngds/api/user
 */
export function getAPIUser() {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: API_USER_GET_START })
    axios.get('/apiUser')
      .then(response => dispatch({ type: API_USER_GET_COMPLETE, payload: response.data }))
      .catch(error => errorHandler(dispatch, error, MESSAGE_ERROR))
  }
}