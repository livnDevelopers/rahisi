import {
  MESSAGE_CLEAR,
} from 'store/actions/types';

/**
 * @name                clearMessage
 * @description         Clears any messages from state.
 */
export function clearMessage() {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
  }
}