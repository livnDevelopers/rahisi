import moment from 'moment';

import axios from 'store/actions/axios-config';
import {
  MESSAGE_CLEAR,
  MESSAGE_ERROR,
  PRODUCT_DEPARTURES_MULTI_START,
  PRODUCT_DEPARTURES_MULTI_COMPLETE,
  PRODUCT_GET_START,
  PRODUCT_GET_COMPLETE,
  PRODUCT_OPTIONS_START,
  PRODUCT_OPTIONS_COMPLETE,
  PRODUCT_PAXDETAILS_START,
  PRODUCT_PAXDETAILS_COMPLETE,
  PRODUCT_PICKUPS_START,
  PRODUCT_PICKUPS_COMPLETE,
  PRODUCT_SELECTIONS_START,
  PRODUCT_SELECTIONS_COMPLETE,
  PRODUCT_TERMS_START,
  PRODUCT_TERMS_COMPLETE
} from 'store/actions/types';
import { errorHandler } from 'store/helpers';

/**
 * @name                getProduct
 * @description         Get the details for a single product by its id.
 * @param   {integer}   productId
 * @param   {object}    options
 * @route               The Livn API route for this is https://*.livngds.com/livngds/api/products/{productId}
 */
export function getProduct(productId, options) {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: PRODUCT_GET_START })
    const { includePickups } = options;
    axios.get('/products/' + productId, {
      params: {
        includePickups
      }
    })
      .then(response => dispatch({ type: PRODUCT_GET_COMPLETE, payload: response.data }))
      .catch(error => errorHandler(dispatch, error, MESSAGE_ERROR))
  }
}

/**
 * @name                getProductDeparturesMulti
 * @description         Performs a departure check for an entire Tour, including where applicable all flavours and/or
 *                      options, from our central cache. Returns operating dates with available units, and if specified
 *                      pricing information for all bookable products under the specified top level tour ID.
 * @param   {integer}   productId
 * @param   {object}    criteria: { endDate, passengers, startDate }
 * @route               The Livn API route for this is https://*.livngds.com/livngds/api/products/{tourId}/departures-multi
 */
export function getProductDeparturesMulti(productId, criteria) {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: PRODUCT_DEPARTURES_MULTI_START })
    axios.get('/products/' + productId + '/departures-multi', {
      params: {
        endDate: criteria && criteria.endDate ? criteria.endDate : moment().add(15, 'days').format('YYYY-MM-DD'),
        minUnits: criteria && criteria.passengers ? criteria.passengers : 1,
        startDate: criteria && criteria.startDate ? criteria.startDate : moment().add(1, 'days').format('YYYY-MM-DD')
      }
    })
      .then(response => dispatch({ type: PRODUCT_DEPARTURES_MULTI_COMPLETE, payload: response.data }))
      .catch(error => errorHandler(dispatch, error, MESSAGE_ERROR))
  }
}

/**
 * @name                getProductPaxDetails
 * @description         Get the required pax details for a single product by its id.
 * @param   {integer}   productId
 * @route               The Livn API route for this is https://*.livngds.com/livngds/api/products/{productId}/paxDetails
 */
export function getProductPaxDetails(productId) {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: PRODUCT_PAXDETAILS_START })
    axios.get('/products/' + productId + '/paxDetails')
      .then(response => dispatch({ type: PRODUCT_PAXDETAILS_COMPLETE, payload: response.data }))
      .catch(error => errorHandler(dispatch, error, MESSAGE_ERROR))
  }
}

/**
 * @name                getProductTerms
 * @description         Get the terms and conditions for a single product by its id, if any are defined.
 * @param   {integer}   productId
 * @route               The Livn API route for this is https://*.livngds.com/livngds/api/products/{productId}/tnc
 */
export function getProductTerms(productId) {
  return function (dispatch) {
    dispatch({ type: MESSAGE_CLEAR })
    dispatch({ type: PRODUCT_TERMS_START })
    axios.get('/products/' + productId + '/tnc')
      .then(response => {
        const { data } = response;
        dispatch({ type: PRODUCT_TERMS_COMPLETE, payload: { object: data, string: !JSON.stringify(data, null, 2) } })
      })
      .catch(error => {
        const { data, status } = error.response;
        /** If there are no terms and conditions for this product, we receive a
         *  response with a 404 status.
         */
        if (status === 404) {
          dispatch({ type: PRODUCT_TERMS_COMPLETE, payload: { object: [], string: '' } })
        } else {
          errorHandler(dispatch, data, MESSAGE_ERROR)
        }
      })
  }
}

/**
 * @name                handleOptions
 * @description         
 * @param   {integer}   passengers
 * @param   {object}    mainProduct
 * @param   {array}     cartItems
 * @param   {function}  history
 * @param   {string}    link
 */
export function handleOptions(passengers, mainProduct, cartItems, history, link) {
  return function (dispatch) {
    dispatch({ type: PRODUCT_OPTIONS_START })
    const paxSelections = [];
    for (let i = 0; i < parseInt(passengers, 10); i++) {
      paxSelections.push({
        paxIndex: i,
        productDate: mainProduct.startDate,
        productId: parseInt(mainProduct.productId, 10)
      })
    }
    for (let i = 0; i < cartItems.length; i++) {
      const { options } = cartItems[i];
      for (let j = 0; j < options.length; j++) {
        const thisOption = options[j];
        if (thisOption.selected) {
          paxSelections.push({
            paxIndex: cartItems[i].paxIndex,
            productDate: thisOption.departure.date,
            productId: thisOption.productId,
            productType: 'OPTION'
          })
        }
      }
    }
    dispatch({ type: PRODUCT_OPTIONS_COMPLETE, payload: paxSelections })
    history.push(link);
  }
}

/**
 * @name                handlePaxSelections
 * @description         If, for some reason, our state has been lost and paxSelections are empty, we are rebuilding them
 *                      with the product id and pickup ids from the route. However, because options are only stored in
 *                      state, they are lost. To better handle this in a production application, one would persist the
 *                      redux state. 
 * @param   {integer}   passengers
 * @param   {object}    mainProduct
 * @param   {string}    pickupList
 */
export function handlePaxSelections(passengers, mainProduct, pickupList) {
  return function (dispatch) {
    dispatch({ type: PRODUCT_SELECTIONS_START })
    const pickups = pickupList.split(',');
    const paxSelections = [];
    for (let i = 0; i < parseInt(passengers, 10); i++) {
      paxSelections.push({
        paxIndex: i,
        pickupId: pickups[i].pickupId,
        productDate: mainProduct.startDate,
        productId: parseInt(mainProduct.productId, 10)
      })
    }
    dispatch({ type: PRODUCT_SELECTIONS_COMPLETE, payload: paxSelections })
  }
}

/**
 * @name                handlePickups
 * @description         
 * @param   {array}     pickups
 * @param   {array}     paxSelections
 * @param   {function}  history
 * @param   {string}    link
 */
export function handlePickups(pickups, paxSelections, history, link) {
  return function (dispatch) {
    dispatch({ type: PRODUCT_PICKUPS_START })
    const pickupList = [];
    pickups.map(pickup => pickupList.push(pickup.pickupId));
    const linkWithPickups = link + '/pickups/' + pickupList.join(',')
    const selectionsWithPickups = [];
    paxSelections.map(selection => {
      selectionsWithPickups.push({
        ...selection,
        pickupId: selection.productType === 'OPTION' ? undefined : pickups[selection.paxIndex].pickupId,
        productType: undefined
      })
    })
    dispatch({ type: PRODUCT_PICKUPS_COMPLETE, payload: selectionsWithPickups })
    history.push(linkWithPickups);
  }
}