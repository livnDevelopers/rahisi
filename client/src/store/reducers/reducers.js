import { combineReducers } from 'redux';

import apiUser from './apiUser';
import cartReducer from './cart';
import dataReducer from './data';
import messageReducer from './message';
import productReducer from './product';
import reservationReducer from './reservation';
import searchReducer from './search';
import userReducer from './user';

const appReducer = combineReducers({
  apiUser: apiUser,
  cart: cartReducer,
  data: dataReducer,
  message: messageReducer,
  product: productReducer,
  reservation: reservationReducer,
  search: searchReducer,
  user: userReducer
})

const rootReducer = (state, action) => {
  return appReducer(state, action);
}

export default rootReducer;