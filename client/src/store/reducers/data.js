import {
  DATA_COUNTRIES_START,
  DATA_COUNTRIES_COMPLETE,
  DATA_LANGUAGES_START,
  DATA_LANGUAGES_COMPLETE
} from 'store/actions/types';
import { updateObject } from 'store/helpers';

const INITIAL_STATE = {
  countries: [],
  languages: [],
  loading: false
}

const dataCountriesComplete = (state, action) => {
  return updateObject(state, { countries: action.payload });
}

const dataLanguagesComplete = (state, action) => {
  return updateObject(state, { languages: action.payload });
}

/**
 * Generic Loading
 */
const dataLoadingStart = (state) => {
  return updateObject(state, { loading: true });
}

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case DATA_COUNTRIES_START: return dataLoadingStart(state);
    case DATA_COUNTRIES_COMPLETE: return dataCountriesComplete(state, action);
    case DATA_LANGUAGES_START: return dataLoadingStart(state);
    case DATA_LANGUAGES_COMPLETE: return dataLanguagesComplete(state, action);
    default: return state;
  }
}