import {
  MESSAGE_CLEAR,
  MESSAGE_ERROR,
  MESSAGE_GET,
  MESSAGE_SUCCESS,
  MESSAGE_WARNING
} from 'store/actions/types';
import { updateObject } from 'store/helpers';

const INITIAL_STATE = {
  text: '',
  type: ''
}

const messageClear = (state) => {
  return updateObject(state, { text: '', type: '' });
}

const messageError = (state, action) => {
  return updateObject(state, { text: action.payload.message, type: 'danger' });
}

const messageGet = (state, action) => {
  return updateObject(state, { text: action.payload.message, type: 'info' });
}

const messageSuccess = (state, action) => {
  return updateObject(state, { text: action.payload.message, type: 'success' });
}

const messageWarning = (state, action) => {
  return updateObject(state, { text: action.payload.message, type: 'warning' });
}

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case MESSAGE_CLEAR: return messageClear(state);
    case MESSAGE_ERROR: return messageError(state, action);
    case MESSAGE_GET: return messageGet(state, action);
    case MESSAGE_SUCCESS: return messageSuccess(state, action);
    case MESSAGE_WARNING: return messageWarning(state, action);
    default: return state;
  }
}