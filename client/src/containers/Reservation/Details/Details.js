import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';

import { getReservation, getReservationTickets } from 'store/actions/actions';

import CodeBlock from 'components/UI/CodeBlock/CodeBlock';
import Hero from 'components/UI/Hero/Hero';
import Notification from 'components/UI/Notification/Notification';

import Detailed from 'components/Reservation/Detailed/Detailed';

class ReservationDetails extends Component {
  componentDidMount() {
    const { getReservation, match: { params: { reservationId } }, reservation: { details } } = this.props;

    /** If we don't already have the reservation details loaded, we're making
     *  sure to get them.
     */
    if (isEmpty(details) || parseInt(details.id, 10) !== parseInt(reservationId, 10)) {
      getReservation(reservationId);
    }
  }
  render() {
    const { getReservationTickets, message, reservation: { details, loading } } = this.props;

    if (!isEmpty(message.text)) {
      return <Notification contained={true} type={message.type}>
        {message.text}
      </Notification>
    }

    if (isEmpty(details) || loading) {
      return <Notification contained={true} spinner={true}>Loading reservation...</Notification>
    }

    return <div>
      <Hero containerClass="has-text-centered" type="primary">
        <h1 className="title is-size-2">Reservation</h1>
        <div className="columns is-size-4">
          <div className="column">
            Reference: {details.globalRef}
          </div>
          <div className="column">
            Total: {details.retailCurrency} {details.grandTotalRetailOverride}
          </div>
        </div>
      </Hero>
      <div className="section">
        <div className="container">
          <div className="columns is-variable is-8">
            <div className="column is-three-fifths">
              <Detailed details={details} getTickets={getReservationTickets} />
            </div>
            <div className="column is-two-fifths">
              <div className="explanation-block content">
                <h3>Reservation details</h3>
                <p>You can get individual reservation details with <code>GET /reservations/&#123;reservationId&#125;</code>. The response for this particular call - for the reservation with an id of <var>{details.id}</var> - is shown below.</p>
                <CodeBlock code={JSON.stringify(details, null, 2)} height="600" language="json" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  }
}

const mapStateToProps = state => ({
  message: state.message,
  reservation: state.reservation
})

export default connect(mapStateToProps, { getReservation, getReservationTickets })(ReservationDetails);