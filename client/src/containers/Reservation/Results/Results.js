import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';

import { clearMessage, getReservationTickets } from 'store/actions/actions';

import CodeBlock from 'components/UI/CodeBlock/CodeBlock';
import Notification from 'components/UI/Notification/Notification';

import Simple from 'components/Reservation/Simple/Simple';

class ReservationResults extends Component {
  componentWillUnmount() {
    const { clearMessage } = this.props;
    clearMessage();
  }

  render() {
    const { getReservationTickets, list, message, reservation: { loading, responseReservationSearch } } = this.props;

    if (!isEmpty(message.text)) {
      return <Notification contained={true} type={message.type}>
        {message.text}
      </Notification>
    }

    if (loading) {
      return <Notification contained={true} spinner={true}>Searching for reservations...</Notification>
    }

    if (!isEmpty(list)) {
      return <div className="columns is-variable is-8">
        <div className="column is-half">
          <div className="margin-bottom-large">
            <Notification>
              <p>Because this application is public, we are restricting the number of reservations returned by this search to the latest 10.</p>
            </Notification>
            {list.map(item => <Simple details={item} getTickets={getReservationTickets} id={item.id} key={item.id} />)}
          </div>
        </div>
        <div className="column is-half">
          <div className="explanation-block content">
            <h3>Viewing reservations</h3>
            <p>Retrieving reservations is as simple as using <code>GET /reservations/reservationId</code> or <code>GET /reservations/reservationId/flatView</code>.</p>
            <CodeBlock code={responseReservationSearch} height="500" language="json" />
          </div>
          <div className="explanation-block content">
            <h3>Getting vouchers for reservations</h3>
            <p>A collated PDF of all vouchers can be obtained through <code>GET /reservations/reservationId/tickets</code>, or a voucher for a specific reservation can be obtained through <code>GET /tickets/reservationItemId</code>.</p>
          </div>
        </div>
      </div>
    } else {
      return <div></div>
    }
  }
}

const mapStateToProps = state => ({
  message: state.message,
  reservation: state.reservation
})

export default connect(mapStateToProps, { clearMessage, getReservationTickets })(ReservationResults);