import React from 'react';

import ReservationForm from 'containers/Reservation/Search/Form';

const ReservationSearch = () => {
  return <div className="columns is-variable is-8">
    <div className="column is-half">
      <ReservationForm />
    </div>
    <div className="column is-half">
    <div className="explanation-block content">
        <h3>Finding reservations</h3>
        <p>It's possible to find a reservation by the passenger's last name or email using <code>GET /reservations/search</code>. Because this also does partial matches and the minimum search term is 3 characters, it's usually easier to search by email address.</p>
        <p>You can also filter the results by providing an optional <code>startDate - (yyyy-MM-ddTHH:mm:ssZ)</code> and <code>endDate - (yyyy-MM-ddTHH:mm:ssZ)</code> that will be matched against <code>reservation.confirmed</code></p>
      </div>
    </div>
  </div>
}

export default ReservationSearch;