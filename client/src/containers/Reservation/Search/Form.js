import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { getReservationSearch } from 'store/actions/actions';

import Button from 'components/UI/Button/Button';
import Input from 'components/UI/Input/Input';

const validationSchema = Yup.object().shape({
  searchValue: Yup.string().min(3).required()
})

class ReservationForm extends Component {
  render() {
    const { getReservationSearch } = this.props;

    return <Formik
      initialValues={{
        searchValue: 'support@livnholidays.com',
        startDate: moment().subtract(5, 'days').startOf('day').format('YYYY-MM-DD'),
        endDate: moment().format('YYYY-MM-DD')
      }}
      validationSchema={validationSchema}
      onSubmit={(values, { setSubmitting }) => {
        getReservationSearch(values)
        setSubmitting(false)
      }}
      render={({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => <form onSubmit={handleSubmit}>
          <Input
            errors={errors.searchValue}
            label="Last name or email address (min. 3 characters)"
            name="searchValue"
            onChange={handleChange}
            onBlur={handleBlur}
            touched={touched.searchValue}
            type="text"
            value={values.searchValue}
          />
          <Input
            errors={errors.startDate}
            label="Start Date (optional)"
            name="startDate"
            onChange={handleChange}
            onBlur={handleBlur}
            touched={touched.startDate}
            type="text"
            value={values.startDate}
          />
          <Input
            errors={errors.endDate}
            label="End Date (optional)"
            name="endDate"
            onChange={handleChange}
            onBlur={handleBlur}
            touched={touched.endDate}
            type="text"
            value={values.endDate}
          />

          <div className="field is-grouped is-grouped-right">
            <div className="control">
              <Button className="is-primary" disabled={isSubmitting} type="submit">
                Search
              </Button>
            </div>
          </div>
        </form>
      }
    />
  }
}

export default connect(null, { getReservationSearch })(ReservationForm);