import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getUser } from 'store/actions/actions';

class Details extends Component {
  componentDidMount() {
    const { getUser, match: { params: { userId } } } = this.props;
    getUser(userId);
  }

  render() {
    return <div>

    </div>
  }
}

const mapStateToProps = state => ({
  user: state.user
})

export default connect(mapStateToProps, { getUser })(Details);