import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getUserList } from 'store/actions/actions';

import Card from 'components/UI/Card/Card';

class List extends Component {
  componentDidMount() {
    const { getUserList } = this.props;
    getUserList();
  }

  render() {
    return <div>
      <Card>
        Contents
      </Card>
    </div>
  }
}

const mapStateToProps = state => ({
  user: state.user
})

export default connect(mapStateToProps, { getUserList })(List);

