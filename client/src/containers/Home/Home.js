import React from 'react';
import { Link } from 'react-router-dom';

import Hero from 'components/UI/Hero/Hero';

import screenshotDocker from 'assets/images/screenshots/docker-001.jpg';
import screenshotExamples from 'assets/images/screenshots/examples-001.jpg';
import screenshotGitLab from 'assets/images/screenshots/gitlab-001.jpg';
import screenshotReact from 'assets/images/screenshots/react-001.jpg';

const Home = () => {
  const columnsClasses = "columns is-variable is-8 is-vcentered";

  return <div>
    <Hero type="light">
      <div className="content">
        <p>Rahisi is a project by our Livn developers for developers. This application will help you to evaluate Livn API as well as help you kick-start your project with fully documented code snippets and full working examples.</p>
        <p>In case you are wondering, Rahisi means easy, facile or inexpensive in Swahili, as Livn developers we are very much into Rahisi.</p>
        <p>We believe that the distribution of tours and activities should be easy, that building a client application becomes facile when you can trust the API, and that starting a whole new project should be inexpensive.</p>
        <p>As fellow coders we also know that documentation is dry and time consuming to read, so we decided to share this demo application project with added spice.</p>
        <p>We have created a base implementation of the LIVN API with help texts and code samples for you to explore. We welcome anyone who is willing to contribute, suggest improvements or likes to get involved in other ways.</p>
      </div>

      <div className="is-hidden-desktop has-text-centered">
        <Link className="button is-primary" to="/login">Login/Register</Link>
      </div>
    </Hero>

    <div className="section">
      <div className="container">
        <div className={columnsClasses}>
          <div className="column">
            <h3 className="is-size-3">Thorough examples &#38; explanations</h3>
            <div className="content">
              <p>Weaved throughout the application are many detailed explanations covering each step from searching for products to looking up existing reservations. These include information on what routes are being used, some tips regarding the usage of different parameters, and ideas about how the data can be handled.</p>

              <p>At certain points, the parameters being sent to the server and/or the response coming back from the server are displayed on the page.</p>
            </div>
          </div>
          <div className="column">
            <img alt="Thorough examples and explanations" className="image" src={screenshotExamples} />
          </div>
        </div>
      </div>
    </div>

    <div className="section">
      <div className="container">
        <div className={columnsClasses}>
          <div className="column">
            <img alt="Built with React and Node" className="image" src={screenshotReact} />
          </div>
          <div className="column">
            <h3 className="is-size-3">
              Built with React &#38; Node
            </h3>
            <div className="content">
              <p>We are using <strong>React</strong> and <strong>Redux</strong> to handle the client. We have put much care and consideration into setting the client up, ensuring that it is simple enough to dive straight into and start learning how to utilise the Livn API in the front end.</p>

              <p><strong>Node</strong> and <strong>Express</strong> are handling the server which sits between the client and the Livn API. It also handles our own set of users - allowing us to create, read, update, and delete them - as well as authentication. Much like the client, we have carefully laid out the server in such a way that it is easy to follow how all the calls are being handled.</p>

              <p>We are using <strong>PostgreSQL</strong> just to handle our own set of users that will be making use of this application - just their names, emails, passwords, and roles. This is not a requirement for using the Livn API, but rather an example of how the Livn API can co-exist with existing systems.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div className="section">
      <div className="container">
        <div className={columnsClasses}>
          <div className="column">
            <h3 className="is-size-3">Publicly available on GitLab</h3>
            <div className="content">
              <p>See this application in action, browse through the code that makes all the magic happen, and run it on your own computer.</p>
            </div>
          </div>
          <div className="column">
            <img alt="Publicly available on GitLab" className="image" src={screenshotGitLab} />
          </div>
        </div>
      </div>
    </div>

    <div className="section">
      <div className="container">
        <div className={columnsClasses}>
          <div className="column">
            <img alt="Easy to run with Docker" className="image" src={screenshotDocker} />
          </div>
          <div className="column">
            <h3 className="is-size-3">Easy to run with Docker</h3>
            <div className="content">
              <p>The client, server, and PostgreSQL database are all Dockerised, allowing this application to be spun up without having to go through a tedious amount of set up. A docker-compose.yml file in the root directory handles all three and the various environment variables that can be set, including the username and password required to access the Livn API.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
}

export default Home;