import React from 'react';

import Hero from 'components/UI/Hero/Hero';

const About = () => {
  return <div>
    <Hero containerClass="has-text-centered" size="medium" type="primary">
      <h1 className="title is-size-1">The Livn API</h1>
      <h3 className="subtitle is-size-3">The Livn API is your one stop shop for all tours and activities</h3>
    </Hero>

    <Hero type="light">
      <div className="content">
        <p>We have unified the live inventory and booking functionality of a large and ever growing number of reservation systems in a single, stable and well documented REST API.</p>
        <p>Connecting with the Livn API thus eliminates the need to develop your own clients for these various services, utilising an array of technologies (REST or REST-ey, SOAP, XMLRPC...), as well as the ongoing maintenance of the implementations.</p>
        <p>In addition Livn, working directly with the tours and activities suppliers, offers you a clean, enriched, comprehensive and uniform data model and set of functionality, surpassing the scope of any one of our individual upstream reservation system APIs.</p>
      </div>
    </Hero>

    <div className="section">
      <div className="container content">
        <ul>
          <li>RESTful web service, supporting both JSON and XML</li>
          <li>Stable API: no non reverse compatible, breaking changes since going live in 2014</li>
          <li>Well maintained documentation, change log and optional API newsletter</li>
          <li>GDPR compliant data policy (European Union General Data Protection Regulation)</li>
          <li>End to end encryption using SSL/TLS 1.2</li>
          <li>Your choice of HTTP authentication or JWT (JSON Web Token)</li>
          <li>Access to both live, uncached availability and rates checks, and Livn’s powerful 365 day in-memory cache</li>
          <li>Fully functional, matching test/development API endpoint and web based test harness</li>
          <li>WADL and Swagger/OpenAPI documents to kick off rapid client development in any language</li>
          <li>Option to receive real-time notifications about relevant inventory updates via Amazon Web Services’ Simple Queue Service (self subscribable via the API)</li>
        </ul>
      </div>
    </div>
  </div>
}

export default About;