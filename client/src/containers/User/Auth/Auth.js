import React from 'react';

import Hero from 'components/UI/Hero/Hero';

import Login from 'containers/User/Login/Login';
import Register from 'containers/User/Register/Register';

const Auth = () => {
  return <div>
    <Hero size="medium" type="primary">
      <h1 className="title is-size-2">Login or Register</h1>
      <h2 className="subtitle is-size-4">Try our demo showing how the Livn API can be implemented</h2>
    </Hero>

    <div className="section">
      <div className="container">
        <div className="columns is-variable is-8">
          <div className="column is-half">
            <Login />
          </div>
          <div className="column is-half">
            <div className="is-hidden-desktop">
              <hr />
            </div>
            <Register />
          </div>
        </div>
      </div>
    </div>
  </div >
}

export default Auth;
