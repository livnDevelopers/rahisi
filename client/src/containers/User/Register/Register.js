import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import { isEmpty } from 'lodash';
import * as Yup from 'yup';

import { postUserRegister } from 'store/actions/actions';

import Button from 'components/UI/Button/Button';
import Input from 'components/UI/Input/Input';
import Notification from 'components/UI/Notification/Notification';

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .email('E-mail is not valid')
    .required('E-mail is required'),
  name: Yup.string()
    .required('Name is required'),
  password: Yup.string()
    .min(6, 'Password has to be longer than 6 characters')
    .required('Password is required'),
  passwordRepeat: Yup.string()
    .min(6, 'Password has to be longer than 6 characters')
    .oneOf([Yup.ref('password')], 'Passwords do not match')
    .required('Password confirmation is required')
})

class Register extends Component {
  render() {
    const { message, postUserRegister } = this.props;

    return <div>
      <h4 className="is-size-4">Register</h4>
      <div className="content">
        <p>It is <strong>not required</strong> to register in order to test this out. There are login details prefilled into the login form which can be used to login.</p>
        <p>Also, on the first of every month, we clear our database of all registrations (except for the test@livn.world account). If you register and find that you can no longer login, this could be the reason.</p>
      </div>
      <Formik
        initialValues={{
          email: '',
          name: '',
          password: '',
          passwordRepeat: ''
        }}
        onSubmit={(values, { setSubmitting }) => {
          postUserRegister(values)
          setSubmitting(false)
        }}
        validationSchema={validationSchema}
        render={({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => <form onSubmit={handleSubmit}>
            <Input
              errors={errors.email}
              horizontal={true}
              label="Email"
              name="email"
              onChange={handleChange}
              onBlur={handleBlur}
              touched={touched.email}
              type="email"
              value={values.email}
            />

            <div className="field is-horizontal">
              <div className="field-label is-normal">
                <label className="label">Password</label>
              </div>
              <div className="field-body">
                <Input
                  errors={errors.password}
                  name="password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  touched={touched.password}
                  type="password"
                  value={values.password}
                />
                <Input
                  errors={errors.passwordRepeat}
                  name="passwordRepeat"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  touched={touched.passwordRepeat}
                  type="password"
                  value={values.passwordRepeat}
                />
              </div>
            </div>

            <Input
              errors={errors.name}
              horizontal={true}
              label="Name"
              name="name"
              onChange={handleChange}
              onBlur={handleBlur}
              touched={touched.name}
              type="text"
              value={values.name}
            />

            {!isEmpty(message.text) ? <Notification type={message.type}>{message.text}</Notification> : null}

            <div className="field is-grouped is-grouped-right">
              <div className="control">
                <Button className="is-text" disabled={isSubmitting} type="reset">
                  Reset
                </Button>
              </div>
              <div className="control">
                <Button className="is-primary" disabled={isSubmitting} type="submit">
                  Submit
                </Button>
              </div>
            </div>
          </form>
        }
      />
    </div>
  }
}

const mapStateToProps = state => ({
  message: state.message
})

export default connect(mapStateToProps, { postUserRegister })(Register);