import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getUser } from 'store/actions/actions';

import Hero from 'components/UI/Hero/Hero';

class Details extends Component {
  componentDidMount() {
    const { getUser, match: { params: { userId } } } = this.props;
    getUser(userId);
  }

  render() {
    const { user: { details } } = this.props;

    return <div>
      <Hero containerClass="has-text-centered" type="success">
        <h1 className="title is-size-2">{details.name}</h1>
        <h2 className="subtitle is-size-4">{details.email}</h2>
      </Hero>

      <div className="section">
        <div className="container">
          <div>
            <strong>Role:</strong> {details.role}
          </div>
        </div>
      </div>

      {/* TODO: <div className="section">
        <div className="container">
          <div className="field is-grouped is-grouped-right">
            <div className="control">
              <Link className="button is-link" to="/">Edit Details</Link>
            </div>
            <div className="control">
              <Link className="button is-link" to="/">Change Password</Link>
            </div>
          </div>
        </div>
      </div> */}
    </div>
  }
}

const mapStateToProps = state => ({
  user: state.user
})

export default connect(mapStateToProps, { getUser })(Details);