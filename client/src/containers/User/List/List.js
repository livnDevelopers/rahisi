import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';

import { getUserList } from 'store/actions/actions';

import Hero from 'components/UI/Hero/Hero';
import Notification from 'components/UI/Notification/Notification';

import UserSimple from 'components/User/Simple/Simple';

class List extends Component {
  componentDidMount() {
    const { getUserList } = this.props;
    getUserList();
  }

  render() {
    const { user: { list } } = this.props;

    if (isEmpty(list)) {
      return <Notification contained={true} spinner={true}>Loading users...</Notification>
    }

    return <div>
      <Hero containerClass="has-text-centered" type="success">
        <h1 className="title is-size-2">All Users</h1>
      </Hero>

      <div className="section">
        <div className="container">
          {list.map(user => <UserSimple details={user} key={user.id} />)}
        </div>
      </div>
    </div>
  }
}

const mapStateToProps = state => ({
  user: state.user
})

export default connect(mapStateToProps, { getUserList })(List);
