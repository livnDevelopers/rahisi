import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';

import { getOperators } from 'store/actions/actions';

import CodeBlock from 'components/UI/CodeBlock/CodeBlock';
import Hero from 'components/UI/Hero/Hero';
import Notification from 'components/UI/Notification/Notification';

import OperatorForm from 'containers/Search/Operator/Form';
import Results from 'containers/Search/Results/Results';

class SearchOperator extends Component {
  componentDidMount() {
    const { getOperators } = this.props;
    getOperators();
  }

  render() {
    const { search: { operators, responseOperator, resultsOperator } } = this.props;

    if (isEmpty(operators)) {
      return <Notification contained={true} spinner={true}>Loading operators...</Notification>
    }

    return <div>
      <Hero type="primary">
        <h1 className="is-size-2">Search by operator</h1>
      </Hero>

      <div className="section">
        <div className="container">
          <div className="explanation-block">
            <div className="columns is-variable is-8">
              <div className="column is-half content">
                <h3>Getting a list of operators</h3>
                <p>We will need to start with <code>GET /operators</code>, so that we can have a list from which the user can select an operator. The list which we have received is shown below.</p>
                <CodeBlock code={responseOperator} height="250" language="json" />
              </div>
              <div className="column is-half content">
                <h3>Searching by operator</h3>
                <p>You can search for products by operator by using <code>GET /operators/&#123;operatorId&#125;/products</code>. There are no filters for this one so availability for a particular product will have to be determined by a separate departures check.</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Hero type="light">
        <OperatorForm />
      </Hero>

      <Results list={resultsOperator} />
    </div>
  }
}

const mapStateToProps = state => ({
  search: state.search
})

export default connect(mapStateToProps, { getOperators })(SearchOperator);