import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';

import { clearMessage } from 'store/actions/actions';

import CodeBlock from 'components/UI/CodeBlock/CodeBlock';
import Notification from 'components/UI/Notification/Notification';

import Simple from 'components/Product/Simple/Simple';

class SearchResults extends Component {
  componentWillUnmount() {
    const { clearMessage } = this.props;
    clearMessage();
  }

  render() {
    const { apiUser: { outletGroup }, message, list, search: { criteria: { passengers, startDate }, loading, responseSearch } } = this.props;

    if (!isEmpty(message.text)) {
      return <Notification contained={true} type={message.type}>
        {message.text}
      </Notification>
    }

    if (isEmpty(outletGroup)) {
      return <Notification contained={true} spinner={true}>Loading...</Notification>
    }

    if (loading) {
      return <Notification contained={true} spinner={true}>Searching for products...</Notification>
    }

    if (!isEmpty(list)) {
      return <div className="columns is-variable is-8" style={{ padding: '3rem' }}>
        <div className="column is-three-fifths">
          <div className="margin-bottom-large">
            <Notification>
              <p>Please be aware that this application only uses a very small, hand-picked selection of products. We made sure to cover a wide range of different scenarios with these products. This will make testing various features across the board without any extensive product knowledge much easier.</p>
            </Notification>
          </div>
          {list.map(item => <Simple criteria={{ passengers, startDate }} currency={outletGroup.currency} key={item.id} product={item} />)}
        </div>
        <div className="column is-two-fifths">
          <div className="explanation-block content">
            <h3>Displaying results</h3>
            <p>There are <strong>two sorting options</strong> through the Livn API - the default is by <var>id</var>, but there is also a random option. In our Node server here however, we have left it set to the default and sorted by <var>duration</var> and then <var>name</var> ourselves.</p>

            <p>Furthermore, the <strong>currency</strong> that we should use when displaying the minimum and maximum prices is that of our Outlet Group. We can either get that ourselves through <code>GET /user</code> or manually set it to whatever we have arranged with Livn. In this sample application, we are using the currency returned from the afforementioned route and we're only using the minimum price to render a "from" amount.</p>

            <CodeBlock code={responseSearch} height="500" language="json" />
          </div>
        </div>
      </div>
    } else {
      return <div></div>
    }
  }
}

const mapStateToProps = state => ({
  apiUser: state.apiUser,
  message: state.message,
  search: state.search
})

export default connect(mapStateToProps, { clearMessage })(SearchResults);