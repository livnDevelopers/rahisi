import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { isEmpty } from 'lodash';
import moment from 'moment';

import { getCart, getCartRetailTotals, getProductTerms } from 'store/actions/actions';

import CodeBlock from 'components/UI/CodeBlock/CodeBlock';
import Hero from 'components/UI/Hero/Hero';
import Notification from 'components/UI/Notification/Notification';

import PaymentForm from 'containers/Cart/Payment/Form';
import Terms from 'containers/Cart/Terms/Terms';

class CartPayment extends Component {
  componentDidMount() {
    const { cart: { details }, getCart, getCartRetailTotals, getProductTerms, match: { params: { cartId, parentId } } } = this.props;

    /** If we don't already have the cart loaded, we're making sure to get it. */
    if (isEmpty(details) || parseInt(details.id, 10) !== parseInt(cartId, 10)) {
      getCart(cartId);
      getCartRetailTotals(cartId);
    }

    getProductTerms(parentId);
  }

  render() {
    const { apiUser: { distributor, outletGroup }, cart: { details, loading, responseRetailTotals, retailTotals }, match: { params: { cartId, parentId } }, product: { responseTerms, terms } } = this.props;

    if (isEmpty(distributor) || isEmpty(outletGroup) || isEmpty(details)) {
      return <Notification contained={true} spinner={true}>Retrieving cart...</Notification>
    }

    if (loading) {
      return <Notification contained={true} spinner={true}>Submitting payment...</Notification>
    }

    return <div>
      <Hero containerClass="has-text-centered" type="primary">
        <h1 className="title is-size-2">{details.cartItems[0].productName}</h1>
        <h2 className="subtitle is-size-4">{moment(details.cartItems[0].productDate).format('dddd, DD MMMM YYYY')}</h2>
      </Hero>
      <div className="section">
        <div className="container">

          <div className="columns is-variable is-8">
            <div className="column is-half">
              <Terms
                distributor={distributor}
                operatorTerms={terms}
              />
              <PaymentForm
                cartId={cartId}
                currency={outletGroup.currency}
                details={retailTotals || details.retailTotals}
                parentId={parentId}
              />
            </div>
            <div className="column">
              <div className="explanation-block content">
                <h3>Getting terms and conditions</h3>
                <p>The terms an conditions for a particular product can be retrieved via <code>GET /products/&#123;productId&#125;/tnc</code> where the <var>productId</var> is that of a bookable product. Keep in mind that if there are no terms and conditions for a product, a 404 response will be returned.</p>
                <p>There is also a set of terms and conditions for the customer from the Livn distributor which can be retrieved from <code>GET /distributor/tnc</code>. However, in this application, we are retrieving that same data via <code>GET /user</code>. Both will need to be agreed upon when making a booking.</p>
                {!isEmpty(responseTerms) ? <CodeBlock code={responseTerms} height="300" language="json" /> : null}
              </div>

              <div className="explanation-block content">
                <h3>Getting retail totals</h3>
                <p>To get the total amount that will need to be paid for the cart, use <code>GET /carts/&#123;cartId&#125;/retailTotals</code>. This will return the gross amount (<var>grossAmount</var>), commission amount (<var>commission</var>), and net amount (<var>netAmount</var>).</p>
                <p>The <var>netAmount</var> is the amount that needs to be sent to Livn, but the amount which the customer will pay is going to be different. The <var>grossAmount</var> is a good guideline as it is based on the price that the operator offers the public.</p>
                <p>See <Link to="/">Pricing and how it's determined</Link> for more details on how these prices might change from any initial estimates.</p>
                {!isEmpty(responseRetailTotals) || !isEmpty(details.retailTotals) ? <CodeBlock code={responseRetailTotals || JSON.stringify(details.retailTotals, null, 2)} language="json" /> : null}
              </div>

              <div className="explanation-block content">
                <h3>Finalising with authorisation or payment</h3>
                <p>After checking out the cart, a credit card payment will be required next. For sending payment of the net amount (<var>netAmount</var>) to Livn, the route is <code>POST /carts/&#123;cartId&#125;/ccPayment</code>.</p>
                <p>However, it is important to remember that these routes are for <strong>submitting payment of the net amount</strong>. Livn is not involved in and does not facilitate the consumer end payment, and you are free to implement whichever payment method or gateway best suits your application's requirements, e.g. Stripe, PayPal, Google/Apple/Ali Pay, etc.</p>
                <p>We do encourage clients using one of the above payment gateways to initially create a pre-authorisation on their customer's credit card over the recommended retail total, or whichever amount you wish to collect, and to convert this pre-authorisation into a debit charge upon successful checkout of your cart. See for instance: <a href="https://stripe.com/docs/charges#auth-capture" rel="noopener noreferrer" target="_blank">https://stripe.com/docs/charges#auth-capture</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  }
}

const mapStateToProps = state => ({
  apiUser: state.apiUser,
  cart: state.cart,
  product: state.product
})

export default connect(mapStateToProps, { getCart, getCartRetailTotals, getProductTerms })(CartPayment);