import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
import moment from 'moment';

import { getCountries, getLanguages, getProductPaxDetails, getProduct, handlePaxSelections } from 'store/actions/actions';

import CodeBlock from 'components/UI/CodeBlock/CodeBlock';
import Hero from 'components/UI/Hero/Hero';
import Notification from 'components/UI/Notification/Notification';

import PaxInfoForm from 'containers/Cart/PaxInfo/Form';

class CartPaxInfo extends Component {
  componentDidMount() {
    const { getCountries, getLanguages, getProductPaxDetails, getProduct, match: { params: { passengers, pickupList, productId, startDate } }, product: { details, paxSelections } } = this.props;

    /** So that we can provide a list of countries and languages within the form
     *  when needed and to ensure consistency, we get a list of each from the Livn
     *  API. 
     */
    getCountries();
    getLanguages();

    /** If we don't already have the product details loaded, we're making sure
     *  to get them. We're setting includePickups to false because we do not need
     *  to load them yet.
     */
    if (isEmpty(details) || parseInt(details.id, 10) !== parseInt(productId, 10)) {
      getProduct(productId, { includePickups: false });
    }

    /** To make sure we know what the required passenger details are at any time,
     *  we're also getting these when the component mounts, based off the id of
     *  the product.
     */
    getProductPaxDetails(productId);

    /** If, for some reason, our state has been lost and paxSelections are empty,
     *  we are rebuilding them with the product id and pickup ids from the route.
     *  However, because options are only stored in state, they are lost. To
     *  better handle this in a production application, one would persist the
     *  redux state. 
     */
    if (isEmpty(paxSelections)) {
      handlePaxSelections(passengers, { productId, startDate }, pickupList)
    }
  }

  render() {
    const { cart: { loading }, data: { countries, languages }, match: { params: { pickupList, passengers, productDate, productId, productType } }, product: { details, paxDetails } } = this.props;

    if (isEmpty(paxDetails) || isEmpty(countries) || isEmpty(languages)) {
      return <Notification contained={true} spinner={true}>Loading passenger details requirements...</Notification>
    }

    if (isEmpty(details) || parseInt(details.id, 10) !== parseInt(productId, 10)) {
      return <Notification contained={true} spinner={true}>Loading...</Notification>
    }

    if (loading) {
      return <Notification contained={true} spinner={true}>Posting the cart...</Notification>
    }

    return <div>
      <Hero containerClass="has-text-centered" type="primary">
        <h1 className="title is-size-2">{details.name}</h1>
        <h2 className="subtitle is-size-4">{moment(productDate).format('dddd, DD MMMM YYYY')}</h2>
      </Hero>

      <div className="section">
        <div className="container">
          <div className="columns is-variable is-8">
            <div className="column is-three-fifths">
              <PaxInfoForm
                passengers={passengers}
                productInfo={{ pickupList, productDate, productId, productType }}
              />
            </div>
            <div className="column is-two-fifths">
              <div className="explanation-block content">
                <h3>Gathering passenger details</h3>
                <p>Depending on the tour operator, different details may be required from the passenger. Futhermore, more details may be required of the lead (first) passenger than the rest.</p>
                <p>To find out what passenger details are required for a certain product, you can use <code>GET /products/&#123;productId&#125;/paxDetails</code>. These details will be important for the cart.</p>
                <p>The <var>paxDetails</var> we are using to display the form on the left are as below.</p>
                <CodeBlock code={JSON.stringify(paxDetails, null, 2)} height="300" language="json" />
                <p>When we post the cart, we will be sending an array of bookable products (<var>cartItems</var>) and passenger details (<var>paxes</var>) separately. Because of this, we need to make sure that the <var>paxIndex</var> used in each <var>cartItems</var> object matches a passenger in the <var>paxes</var> array. We also need to make sure that there are no gaps in the numbers used as <var>paxIndex</var>. For example, we cannot have <var>paxIndexes</var> of <var>0</var>, <var>2</var>, and <var>4</var>.</p>
                <p>Some other notes regarding passenger details:</p>
                <ul>
                  <li>The optional salutation expects 'MR', 'MRS', 'MISS', or 'MS'.</li>
                  <li><code>GET /public/countries</code> and <code>GET /public/languages</code> can be used to get lists for country, language, and nationality.</li>
                  <li>The passport requirement means the passport number (<var>passportNumber</var>), issued date (<var>passportIssued</var>), and expiry date (<var>passportExpiry</var>) are required.</li>
                  <li>The address requirement means the street address (<var>address1</var>), postcode (<var>postcode</var>), city (<var>city</var>), and state (<var>state</var>) are required.</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  }
}

const mapStateToProps = state => ({
  cart: state.cart,
  data: state.data,
  product: state.product
})

export default connect(mapStateToProps, { getCountries, getLanguages, getProductPaxDetails, getProduct, handlePaxSelections })(CartPaxInfo);