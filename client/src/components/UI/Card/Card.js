import React from 'react';
import { Link } from 'react-router-dom';
import { isEmpty } from 'lodash';

import { hashFnv32a } from 'utilities/general';

const Card = ({ children, footer, image, title }) => {
  return <div className="card">
    {title ? <header className="card-header">
      <p className="card-header-title">{title}</p>
    </header> : null}
    {!isEmpty(image) ? <div className="card-image">
      <figure className={`image is-${image.size}`}>
        <img src={image.src} alt={image.alt} />
      </figure>
    </div> : null}
    <div className="card-content">
      <div className="content">
        {children}
      </div>
    </div>
    {!isEmpty(footer) ? <footer className="card-footer">
      {footer.map(footerItem => <Link to={footerItem.to} className="card-footer-item" key={hashFnv32a(footerItem.to + '-' + footerItem.text, true)}>{footerItem.text}</Link>)}
    </footer> : null}
  </div>
}

export default Card;