import React from 'react';

const renderOption = (option, optionText, optionValue) => {
  let text = option.text;
  if (optionText) {
    const splitOT = optionText.split(".");
    text = splitOT.length > 1 ? option[splitOT[0]][splitOT[1]] : option[optionText];
  }

  let value = option.value;
  if (optionValue) {
    const splitOV = optionValue.split(".");
    value = splitOV.length > 1 ? option[splitOV[0]][splitOV[1]] : option[optionValue];
  }

  return <option key={value} value={value}>
    {text}
  </option>
}

const renderControl = ({ disabled, errors, name, onChange, onBlur, options, optionText, optionValue, touched, value }, validationClass) => <div className="control">
  <div className={`select ${validationClass} is-fullwidth`}>
    <select
      disabled={disabled}
      name={name}
      onChange={onChange}
      onBlur={onBlur}
      value={value}>
      {options.map(option => renderOption(option, optionText, optionValue))}
    </select>
    {touched && errors ? <div className="help is-danger">{errors}</div> : null}
  </div>
</div>

const Select = ({ disabled, errors, fieldClass, label, name, onChange, onBlur, options, optionText, optionValue, touched, value }) => {
  let validationClass = '';
  if (touched && errors) {
    validationClass = 'is-danger';
  } else if (!errors && value) {
    validationClass = 'is-success';
  }

  return <div className={`field ${fieldClass && fieldClass}`}>
    {label ? <label className="label">{label}</label> : null}
    {renderControl({ disabled, errors, name, onChange, onBlur, options, optionText, optionValue, touched, value }, validationClass)}
  </div>
}

export default Select;