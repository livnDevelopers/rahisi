"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    "hljs-comment": {
        "color": "#8e908c"
    },
    "hljs-quote": {
        "color": "#8e908c"
    },
    "hljs-variable": {
        "color": "#ff5959"
    },
    "hljs-template-variable": {
        "color": "#ff5959"
    },
    "hljs-tag": {
        "color": "#ff5959"
    },
    "hljs-name": {
        "color": "#ff5959"
    },
    "hljs-selector-id": {
        "color": "#ff5959"
    },
    "hljs-selector-class": {
        "color": "#ff5959"
    },
    "hljs-regexp": {
        "color": "#ff5959"
    },
    "hljs-deletion": {
        "color": "#ff5959"
    },
    "hljs-number": {
        "color": "#ffac00"
    },
    "hljs-built_in": {
        "color": "#ffac00"
    },
    "hljs-builtin-name": {
        "color": "#ffac00"
    },
    "hljs-literal": {
        "color": "#ffac00"
    },
    "hljs-type": {
        "color": "#ffac00"
    },
    "hljs-params": {
        "color": "#ffac00"
    },
    "hljs-meta": {
        "color": "#ffac00"
    },
    "hljs-link": {
        "color": "#ffac00"
    },
    "hljs-attribute": {
        "color": "#eab700"
    },
    "hljs-string": {
        "color": "#5dae5d"
    },
    "hljs-symbol": {
        "color": "#5dae5d"
    },
    "hljs-bullet": {
        "color": "#5dae5d"
    },
    "hljs-addition": {
        "color": "#5dae5d"
    },
    "hljs-title": {
        "color": "#00aeef"
    },
    "hljs-section": {
        "color": "#00aeef"
    },
    "hljs-keyword": {
        "color": "#8959a8"
    },
    "hljs-selector-tag": {
        "color": "#8959a8"
    },
    "hljs": {
        "display": "block",
        "overflowX": "auto",
        "background": "white",
        "color": "#4a4a4a",
        "padding": "0.5em"
    },
    "hljs-emphasis": {
        "fontStyle": "italic"
    },
    "hljs-strong": {
        "fontWeight": "bold"
    }
};