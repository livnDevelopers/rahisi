import React from 'react';

const Checkbox = (props) => {
  const { className, errors, disabled, label, name, onBlur, onChange, touched, value } = props;

  let validationClass = '';
  if (touched && errors) {
    validationClass = 'is-danger';
  } else if (!errors && value) {
    validationClass = 'is-success';
  }

  return <div className={className}>
    <label className="label">
      <span style={{ marginRight: '5px' }}>{label}</span>
      <input
        className={validationClass}
        disabled={disabled}
        name={name}
        onChange={onChange}
        onBlur={onBlur}
        type="checkbox"
        value={value}
      />
    </label>
    {touched && errors ? <div className="help is-danger">{errors}</div> : null}
  </div>
}

export default Checkbox;