import React from 'react';

const Media = ({ children, icon: { alt, size, src } }) => {
  return <article className="media">
    <div className="media-left">
      <img alt={alt} className={`image is-${size || '64x64'}`} src={src} />
    </div>
    <div className="media-content">
      <div className="content">
        {children}
      </div>
    </div>
  </article>
}

export default Media;