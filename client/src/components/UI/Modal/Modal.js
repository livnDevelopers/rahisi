import React from 'react';

const Modal = ({ children, closeable, image }) => {
  return <div className="modal">
    <div className="modal-background"></div>
    <div className="modal-content">
      {image ? <div className="image is-4by3">
        <img alt={image.alt} src={image.src} />
      </div> : children}
    </div>
    {closeable ? <button className="modal-close is-large" aria-label="close"></button> : null}
  </div>
}

export default Modal;
