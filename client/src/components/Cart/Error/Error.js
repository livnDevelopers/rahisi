import React from 'react';
import { Link } from 'react-router-dom';

import CodeBlock from 'components/UI/CodeBlock/CodeBlock';
import Hero from 'components/UI/Hero/Hero';
import Notification from 'components/UI/Notification/Notification';

const Error = ({ message, parentId, responseError }) => {
  return <div>
    <Hero type="light">
      <Notification type={message.type}>{message.text}</Notification>
    </Hero>
    <div className="section">
      <div className="container">
        <div className="columns is-variable is-8">
          <div className="column is-half">
            <div className="explanation-block content">
              <p>It looks like something went wrong with our cart. The response is shown below and to the right are some explanations of what has happened and what other errors could pop up. We have not implemented any workarounds for these errors in this sample application, so you may have to go back to the product and make some different selections or choose a different base product altogether.</p>
              <CodeBlock code={responseError} height="925" language="json" />
              <div className="field is-grouped margin-top">
                <div className="control">
                  <Link className="button is-link" to={'/be/product/details/tour/' + parentId}>Back to product details</Link>
                </div>
              </div>
            </div>
          </div>
          <div className="column is-half">
            <div className="explanation-block content">
              <h3>Cart statuses</h3>
              <p>More details for these cart statuses can usually be found in the returned cart under problems.</p>
              <dl>
                <dt><var>EXPIRED</var></dt>
                <dd>A cart, that passed the initial validation and live checks phase, but was not taken to checkout in the allowed time (default: 15 minutes). <u>Next action:</u> Cart becomes obsolete.</dd>
                <dt><var>FAILED_CHECKOUT</var></dt>
                <dd>A problem occurred during checkout (<code>GET /carts/&#123;cartId&#125;/checkout</code>). <u>Next action:</u> Cart becomes obsolete.</dd>
                <dt><var>FAILED_LIVE_CHECKS</var></dt>
                <dd>Signalises a problem during validation or the rates and availability checks. <u>Next action:</u> Cart becomes obsolete.</dd>
                <dt><var>TIMED_OUT_AFTER_CHECKOUT</var></dt>
                <dd>The transaction has been rolled back and external reservations have been cancelled, because the pending authorisation/re-confirmation was not given in time. <u>Next action:</u> Cart becomes obsolete.</dd>
              </dl>
            </div>
            <div className="explanation-block content">
              <h3>Cart problem codes</h3>
              <dl>
                <dt><var>UNSPECIFIED</var></dt>
                <dd>This covers instances not already covered by one of the below codes.</dd>
                <dt><var>CART_ITEM</var></dt>
                <dd>A problem with at least one of the cart's items. See individual cart items' problems section for full details.</dd>
                <dt><var>CART_ITEM_AVAILABILITY_CHECK_FAILED</var></dt>
                <dd>Could not check the cart item's availability.</dd>
                <dt><var>CART_ITEM_INSUFFICIENT_AVAILABILITY</var></dt>
                <dd>There is not enough availability for the selected cart item.</dd>
                <dt><var>CART_ITEM_PRICE_CHECK_FAILED</var></dt>
                <dd>Could not retrieve the cart item's up-to-date pricing information.</dd>
                <dt><var>CART_ITEM_PRICE_CHECK_NO_VALID_RATES</var></dt>
                <dd>Could not retrieve valid rates for this cart item</dd>
                <dt><var>CART_ITEM_PRICE_CHECK_CALCULATING_WHOLESALE_COMMISSION_FAILED</var></dt>
                <dd>Could not calculate the wholesale commission for this cartitem.</dd>
                <dt><var>CART_ITEM_CURRENCY_CONVERSION_FAILED</var></dt>
                <dd></dd>
                <dt><var>CART_CREDIT_CARD_PAYMENT_FAILED</var></dt>
                <dd>Credit card payment for checked out cart failed</dd>
                <dt><var>VALIDATION</var></dt>
                <dd>Cart items could not be grouped into upstream booking requests and validated against the respective operator's requirements.</dd>
              </dl>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
}

export default Error;