import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';

const Departure = ({ currency, details, parentId, passengers, productId, productType }) => {
  const { availableUnits, date, departureRates: { retailCommissionAmountAdult, retailCommissionAmountChild, retailNetRateAdult, retailNetRateChild, retailRateAdult, retailRateChild } } = details;
  let link = '/be/product/options/tour/';
  if (productType === 'flavour') {
    link += parentId + '/' + productType + '/' + productId;
  } else {
    link += productId;
  }
  link += '/' + date + '/' + passengers;

  return <div>
    <div className="columns" style={{ marginBottom: 0 }}>
      <div className="column is-two-fifths">
        <strong>{moment(date).format('DD MMMM YYYY')}</strong>
      </div>
      <div className="column">
        Status: <span className="has-text-success">{details.status}</span>
      </div>
      <div className="column">
        Available Units: {availableUnits}
      </div>
    </div>
    <div className="columns has-text-right" style={{ marginBottom: 0 }}>
      <div className="column">
        <div><u>Net Rates</u></div>
        {retailNetRateAdult ? <div>Adults: {currency} {retailNetRateAdult}</div> : null}
        {retailNetRateChild ? <div>Children: {currency} {retailNetRateChild}</div> : null}
      </div>
      <div className="column">
        <div><u>Commission Rates</u></div>
        {retailCommissionAmountAdult ? <div>Adults: {currency} {retailCommissionAmountAdult}</div> : null}
        {retailCommissionAmountChild ? <div>Children: {currency} {retailCommissionAmountChild}</div> : null}
      </div>
      <div className="column">
        <div><u>Gross Rates</u></div>
        {retailRateAdult ? <div>Adults: {currency} {retailRateAdult}</div> : null}
        {retailRateChild ? <div>Children: {currency} {retailRateChild}</div> : null}
      </div>
    </div>
    <div className="field is-grouped is-grouped-right margin-right margin-bottom">
      <div className="control">
        <Link className="button is-primary" style={{ marginRight: '-2px' }} to={link}>Select {moment(date).format('DD MMMM YYYY')}</Link>
      </div>
    </div>
  </div>
}

export default Departure;