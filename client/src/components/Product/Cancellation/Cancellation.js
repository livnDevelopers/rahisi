import React from 'react';
import moment from 'moment';

import { hashFnv32a } from 'utilities/general';

const renderCancellation = ({ feePerc, hoursTillDeparture }) => <li key={hashFnv32a(hoursTillDeparture + '-' + feePerc, true)}>Within {moment.duration(hoursTillDeparture, 'hours').humanize()} of departure: {feePerc}% of the total cost</li>

const Cancellation = ({ list }) => {
  return <div className="content">
    <ul style={{ marginTop: 0 }}>
      {list.map(item => renderCancellation({ feePerc: item.feePerc, hoursTillDeparture: item.hoursTillDeparture }))}
    </ul>
  </div>
}

export default Cancellation;