import React from 'react';
import { isEmpty } from 'lodash';
import moment from 'moment';

import Levy from 'components/Product/Levy/Levy';
import Cancellation from 'components/Product/Cancellation/Cancellation';

const Item = ({ currency, details, fullDetails }) => {
  const { paxName, productName } = details;

  const pickupPoint = details.pickupPointName ? fullDetails ? <div>
    <span className="has-text-grey">Pickup point:</span>&nbsp;{details.pickupPointName}, {details.pickupTime ? moment(details.pickupTime, 'HH:mm:ss.SS').format('HH:mm').toString() : null} {details.pickupAddress}
  </div> : <div>
      <span className="has-text-grey">Pickup point:</span>&nbsp;{details.pickupPointName}
    </div> : null;

  const levies = !isEmpty(details.levies) ? <div className="column">
    <span className="has-text-grey">Levies:</span>&nbsp;<Levy currency={currency} list={details.levies} />
  </div> : null;

  const cancellationPolicy = !isEmpty(details.cancellationPolicy) ? <div className="column">
    <span className="has-text-grey">Cancellation Policy:</span>&nbsp;<Cancellation list={details.cancellationPolicy} />
  </div> : null;

  const extraDetails = fullDetails ? [<div className="columns" key="rates">
    <div className="column">
      <span className="has-text-grey">Retail Rate:</span>&nbsp;{currency} {details.retailRate}
    </div>
    <div className="column">
      <span className="has-text-grey">Commission Amount:</span>&nbsp;{currency} {details.retailCommissionAmount}
    </div>
  </div>,
  <div className="columns is-size-7" key="misc">
    {levies}
    {cancellationPolicy}
  </div>] : null;

  return <div className={fullDetails ? "has-background-white content margin-bottom-large padded-large" : "margin-bottom"}>
    <div className="columns">
      <div className="column">
        <div className="is-size-5">
          {paxName}
        </div>
        <hr />
        <div>
          <span className="has-text-grey">Date:</span>&nbsp;{moment(details.date).format('DD/MM/YYYY')}
        </div>
        <div>
          <span className="has-text-grey">Product:</span>&nbsp;{productName}
        </div>
        {pickupPoint}
      </div>
    </div>
    {extraDetails}
  </div>
}

export default Item;