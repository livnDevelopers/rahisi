import React from 'react';
import { Link } from 'react-router-dom';

import Button from 'components/UI/Button/Button';

import Item from 'components/Reservation/Item/Item';

const Simple = ({ details, getTickets, id }) => {
  const { globalRef, grandTotalRetailOverride, numberOfPax, reservationItems, retailCurrency, status } = details;
  let statusClass = '';
  switch (status) {
    case 'CONFIRMED': {
      statusClass = 'has-text-success';
      break;
    }
    case 'CANCELLED': {
      statusClass = 'has-text-danger';
      break;
    }
    default: {
      statusClass = '';
      break;
    }
  }

  return <div className="has-background-white margin-bottom-large padded-large">
    <div className="columns is-desktop">
      <div className="column">
        Reference: {globalRef}
      </div>
      <div className="column">
        Status: <span className={statusClass}>{status}</span>
      </div>
      <div className="column">
        {numberOfPax} passengers
        </div>
    </div>

    {reservationItems.map(item => <Item details={item} fullDetails={false} key={item.id} />)}

    <div className="columns is-desktop has-text-centered">
      <div className="column">
        Total: {retailCurrency} {grandTotalRetailOverride}
      </div>
      <div className="column">
        <Button className="is-primary" clickEvent={() => {
          getTickets(id)
        }} type="button">
          Download tickets
        </Button>
      </div>
      <div className="column">
        <Link className="button is-link" to={'/be/reservations/' + details.id}>
          More details
        </Link>
      </div>
    </div>
  </div>
}

export default Simple;