import React from 'react';
import { Link } from 'react-router-dom';

import Button from 'components/UI/Button/Button';

import Item from 'components/Reservation/Item/Item';

const Detailed = ({ details: { id, reservationItems, retailCurrency }, getTickets }) => {
  return <div>
    {reservationItems.map(item => <Item currency={retailCurrency} details={item} fullDetails={true} key={item.id} />)}

    <div className="field is-grouped is-grouped-right margin-top">
      <div className="control">
        <Link className="button is-link" to="/be/reservations">Back to Reservations</Link>
      </div>
      <div className="control">
        <Button className="is-primary" clickEvent={() => { getTickets(id) }} type="button">Download tickets</Button>
      </div>
    </div>
  </div>
}

export default Detailed;