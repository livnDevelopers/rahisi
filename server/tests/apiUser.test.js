const request = require('supertest');
let sandbox = require('sinon').createSandbox();
const config = require('../config/config');
const userConstants = require('../constants/user');
const jwt = require('jsonwebtoken');

const rewire = require('rewire');
let apiUserController = rewire('../controllers/apiUser');
let axios = apiUserController.__get__('axios');

const models = require('../models');
const User = require('../models/index').User;

const app = require('../app');


function generateToken(userId) {
  return 'Bearer ' + jwt.sign({sub: userId}, config.jwtSecret, {expiresIn: userConstants.TOKEN_EXPIRY});
}


describe('apiUser Tests', function () {
  beforeEach(async function () {
    // Removing tables
    await models.sequelize.sync({force: true, logging: false});

    // Seeding users
    await User.create({
      email: 'admin@test.com',
      name: 'Test Admin',
      password: 'password',
      role: userConstants.ROLE.USER
    });
  });

  afterEach(function () {
    sandbox.restore();
  });


  describe('GET /apiUser', function () {
    it('should call axios method', async function () {
      //Stub axios
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      await request(app)
        .get('/api/apiUser')
        .set('Authorization', token);
      sandbox.assert.calledOnce(axios.get);
    });
  })
});
