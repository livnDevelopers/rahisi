const expect = require('chai').expect;
const request = require('supertest');
let sandbox = require('sinon').createSandbox();
const config = require('../config/config');
const userConstants = require('../constants/user');
const jwt = require('jsonwebtoken');

const rewire = require('rewire');
let reservationController = rewire('../controllers/reservation.js');
let axios = reservationController.__get__('axios');

const models = require('../models');
const User = require('../models/index').User;

const app = require('../app');


function generateToken(userId) {
  return 'Bearer ' + jwt.sign({sub: userId}, config.jwtSecret, {expiresIn: userConstants.TOKEN_EXPIRY});
}


describe('Reservation Tests', function () {
  beforeEach(async function () {
    // Removing tables
    await models.sequelize.sync({force: true, logging: false});

    // Seeding users
    await User.create({
      email: 'admin@test.com',
      name: 'Test Admin',
      password: 'password',
      role: userConstants.ROLE.USER
    });
  });

  afterEach(function () {
    sandbox.restore();
  });


  describe('GET /reservations/search', function () {
    it('should call axios method if query parameter is valid', async function () {
      //Stub axios
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      await request(app)
        .get('/api/reservations/search?searchValue=abcd')
        .set('Authorization', token);
      sandbox.assert.calledOnce(axios.get);
    });

    it('should not call axios method if query parameter is less than 3 characters long', async function () {
      //Stub axios
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      const response = await request(app)
        .get('/api/reservations/search?searchValue=ab')
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.get);
    });
  })


  describe('GET /reservations/:reservationId', function () {
    it('should call axios method if parameter is valid', async function () {
      //Stub axios
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      await request(app)
        .get('/api/reservations/1')
        .set('Authorization', token);
      sandbox.assert.calledOnce(axios.get);
    });

    it('should not call axios method if parameter is invalid', async function () {
      //Stub axios
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      const response = await request(app)
        .get('/api/reservations/a')
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.get);
    });
  })


  describe('GET /reservations/:reservationId/tickets', function () {
    it('should call axios method if parameter is valid', async function () {
      //Stub axios
      sandbox.stub(axios, 'get').returns({
        response: {
          data: {
            test: 'test'
          }
        }
      });

      const token = generateToken(1).toString();
      await request(app)
        .get('/api/reservations/1/tickets')
        .set('Authorization', token);
      sandbox.assert.calledOnce(axios.get);
    });

    it('should not call axios method if parameter is invalid', async function () {
      //Stub axios
      sandbox.stub(axios, 'get');

      const token = generateToken(1).toString();
      const response = await request(app)
        .get('/api/reservations/a/tickets')
        .set('Authorization', token);
      expect(response.statusCode).to.equal(422);
      sandbox.assert.notCalled(axios.get);
    });
  })
});
