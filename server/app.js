const cors = require('cors');
const express = require('express');
const passport = require('passport');
const helmet = require('helmet');

// Configuring express-validation for the controllers
const ev = require('express-validation');
// assign express-validation options
ev.options({
  status: 422,
  statusText: 'Unprocessable Entity',
  allowUnknownBody: false,
  allowUnknownHeaders: false,
  allowUnknownQuery: false,
  allowUnknownParams: false,
  allowUnknownCookies: false
});

const errorMiddleware = require('./middleware/errorHandler');

const router = require('./routes/router');

const app = express();

app.use(cors());
app.use(helmet());
app.enable('trust proxy');
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use(passport.initialize());

router(app);

// Error handler middleware
app.use(errorMiddleware.logErrors);
app.use(errorMiddleware.errorHandler);

module.exports = app;
