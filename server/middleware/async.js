const boom = require('boom');

/**
 * 
 * asyncMiddleware is a function that takes another function and wraps it in a promise. In our use case the function it will take is an express route handler, 
 * and since we are passing that handler into Promise.resolve it will resolve with whatever value our route handler returns. 
 * If, however, one of the await statements in our handler gives us a rejected promise, the error will be passed to next which will eventually give 
 * the error to our express error middleware to handle.
 */
const asyncMiddleware = fn => (req, res, next) => {
  Promise.resolve(fn(req, res, next)).catch((err) => {
    if (!err.isBoom) {
      return next(boom.badImplementation(err));
    }
    next(err);
  });
};

module.exports = {
  asyncMiddleware
};