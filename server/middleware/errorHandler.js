const boom = require('boom');
const {logError} = require('../config/logger');

/**
 * Middleware used to log errors.
 * 
 * boom library: boom provides a set of utilities for returning HTTP errors.
 * 
 * If the errors are validation errors from the controllers (status code = 422), we compile them into an array and we use boom to create a 
 * custom error that will be sent to the client.
 * The error gets logged and is sent to the error handler.
 */
module.exports.logErrors = (err, req, res, next) => {
  if (!boom.isBoom(err) && err.status === 422) {
    let errors = [];
    // building up an array with all the validation errors descriptions
    for (let error of err.errors) {
      for (let errorMsg of error.messages) {
        errors.push(errorMsg);
      }
    }
    // overwriting the error with a boom instance with the same status code (422)
    err = new boom(errors, {statusCode: err.status});
  } else if (!boom.isBoom(err)) {
    err = new boom(err.message);
  }
  let errorInfo = {
    name: err.output.payload.error,
    message: err.output.payload.message,
    stack: err.stack
  };
  if (req) {
    errorInfo.userAgent = req.get('User-Agent');
    if (req.user) {
      errorInfo.userDetails = 'id: ' + req.user.id + ', email: ' + req.user.email;
    }
    errorInfo.ip = req.ip;
    errorInfo.method = req.method;
    errorInfo.url = req.protocol + '://' + req.get('host') + req.originalUrl;
  }
  logError.error(errorInfo);
  next(err);
};


/**
 * Middleware used to handle errors. 
 * The handler returns the statusCode and the error message/payload to the client.
 */
module.exports.errorHandler = (err, req, res, next) => {
  return res.status(err.output.statusCode).json(err.output.payload);
};
