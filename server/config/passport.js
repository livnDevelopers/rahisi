const JwtStrategy = require('passport-jwt').Strategy;
const LocalStrategy = require('passport-local').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const config = require('../config/config');
const User = require('../models/index').User;

const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = config.jwtSecret;

module.exports = passport => {
  /**
   * Passport JWT strategy
   * 
   * This strategy lets you authenticate endpoints using a JSON web token. It is intended to be used to secure RESTful endpoints without sessions.
   * For more information, visit http://www.passportjs.org/packages/passport-jwt/ 
   */
  passport.use(new JwtStrategy(opts, async (jwt_payload, done) => {
    try {
      const user = await User.findById(jwt_payload.sub) //sub is used to store the user Id
      if (user) {
        return done(null, user);
      }
      return done(null, false);
    } catch (err) {
      return done(null, false);
    }
  })),

    /**
     * Passport local strategy
     * 
     * This strategy lets you authenticate using a username and password
     * For more information, visit http://www.passportjs.org/packages/passport-local/ 
     */
    passport.use(new LocalStrategy({
      usernameField: 'email', // need to map userName field to email, defaults to 'username'
      passwordField: 'password',
      session: false // disabled session support
    }, async (email, password, done) => {
      try {
        const user = await User.findOne({ where: { email: email.toLowerCase() } });
        // When user is not found
        if (!user) return done(null, false);
        // When password is not correct
        if (!(await user.verifyPassword(password))) return done(null, false);
        // When all things are good, we return the user
        return done(null, user);
      } catch (err) {
        return done(null, false);
      }
    }));
}