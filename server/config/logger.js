const {format, transports, loggers} = require('winston');
const {printf} = format;
const moment = require('moment-timezone');

const myFormat = printf(info => {
  info.timestamp = moment(info.timestamp).tz('Australia/Sydney').format('YYYY-MM-DD | HH:mm:ss');
  info.meta = undefined;
  return JSON.stringify(info);
});

loggers.add('error', {
  transports: [
    new (transports.Console)({
      level: 'debug',
      format: format.combine(
        format.splat(),
        myFormat
      )
    })
  ]
});

loggers.add('other', {
  transports: [
    new (transports.Console)({
      level: 'debug',
      format: format.combine(
        format.splat(),
        myFormat
      )
    })
  ]
});

module.exports = {
  logError: loggers.get('error'),
  logger: loggers.get('other'),
};