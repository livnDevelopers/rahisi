module.exports = {

  // Routes used to connect to the LIVN API
  OPERATORS_ROUTE: 'https://dev1.livngds.com/livngds/api/operators',
  PRODUCTS_ROUTE: 'https://dev1.livngds.com/livngds/api/products/',
  PUBLIC_ROUTE: 'https://dev1.livngds.com/livngds/api/public/',
  CARTS_ROUTE: 'https://dev1.livngds.com/livngds/api/carts',
  APIUSER_ROUTE: 'https://dev1.livngds.com/livngds/api/user',
  RESERVATIONS_ROUTE: 'https://dev1.livngds.com/livngds/api/reservations/',

  // Default timeout for all the server routes connecting to the API - 2 minutes
  TIMEOUT_120: 120000,
}