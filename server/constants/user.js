module.exports = {
  // Available user roles
  ROLE: {
    ADMINISTRATOR: 'ADMINISTRATOR',
    USER: 'USER'
  },
  TOKEN_EXPIRY: 3600,
}
