const express = require('express');
const router = express.Router();

const publicController = require('../../controllers/public');

/**
 * @route         GET api/public/countries
 * @description   Retrieves a list of countries
 * @access        Public
 */
router.get('/countries', publicController.countries);

/**
 * @route         GET api/public/languages
 * @description   retrieves a list of available languages
 * @access        Public
 */
router.get('/languages', publicController.languages);

/**
 * @route         GET api/public/time
 * @description   retrieves the current date of the server
 * @access        Public
 */
router.get('/time', publicController.time);

module.exports = router;
