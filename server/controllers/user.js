const jwt = require('jsonwebtoken');
const config = require('../config/config');
const User = require('../models/index').User;
const userConstants = require('../constants/user');
const boom = require('boom');
const {asyncMiddleware} = require('../middleware/async');
const {logger} = require('../config/logger');


/**
 * Registers a new user in the database.
 *
 * @param req {Object} - The request.
 * @param res {Object} - The response. 
 * @param req.body.email {string} - email of the user
 * @param req.body.name {string} - name of the user
 * @param req.body.password {string} - un-encrypted user password
 * @param req.body.role {string} - role of the user (hard coded from the client to USER)
 * 
 * @returns JSON message about successful registration
 */
exports.create = asyncMiddleware(async (req, res) => {
  const user = await User.findOne({where: {email: req.body.email.toLowerCase()}});
  if (user) {
    throw boom.preconditionFailed('There is already an account with that email address.');
  }
  await User.create({
    email: req.body.email,
    name: req.body.name,
    password: req.body.password,
    role: req.body.role
  });
  logger.info({message: 'User registered', email: req.body.email, ip: req.ip});
  return res.status(201).send({message: 'You have been registered'});
});


/**
 * Lists all the users currently in the database.
 *
 * @param req {Object} - The request.
 * @param res {Object} - The response. 
 * 
 * @returns a list of user objects excluding the password and timestamps
 */
exports.list = asyncMiddleware(async (req, res) => {
  const users = await User.findAll({
    raw: true,
    attributes: {exclude: ['password', 'createdAt', 'updatedAt']}
  });
  return res.send(users);
});

/**
 * Signs a user into the system
 *
 * @param req {Object} - The request.
 * @param res {Object} - The response.
 * @param next {Function} - Express next middleware function
 * @param req.user {object} - after passport evaluates login details, it creates req.user which includes all the user details 
 *  
 * @returns The user object
 */
exports.login = (req, res, next) => {
  const token = jwt.sign({sub: req.user.id}, config.jwtSecret, {expiresIn: userConstants.TOKEN_EXPIRY});
  if (!token) {
    return next(boom.badRequest(`Token error: ${error.message}`));
  }
  logger.info({message: 'User logged in', id: req.user.id, email: req.user.email, ip: req.ip});
  return res.send({
    token: 'Bearer ' + token,
    tokenExpiry: userConstants.TOKEN_EXPIRY,
    user: {
      email: req.user.email,
      id: req.user.id,
      name: req.user.name,
      role: req.user.role
    }
  });
};

/**
 * Returns a user object by id
 *
 * @param req {Object} - The request.
 * @param res {Object} - The response.
 * @param req.params.userId {integer} - id of the user 
 * 
 * @returns The user object
 */
exports.retrieve = asyncMiddleware(async (req, res) => {
  const user = await User.findByPk(req.params.userId, {
    raw: true,
    attributes: {exclude: ['password', 'createdAt', 'updatedAt']}
  });
  if (!user) {
    throw boom.badRequest('user Not Found');
  }
  logger.info({message: 'User loaded', id: user.id, email: user.email, ip: req.ip});
  return res.send(user);
});
