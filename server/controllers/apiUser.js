const config = require('../config/config');
const axios = require('axios');
const apiRoutes = require('../constants/apiRoutes');
const boom = require('boom');

/**
 * @param req {Object} - The request.
 * @param res {Object} - The response. 
 * @param next {Function} - Express next middleware function
 * 
 * @returns the apiuser object.
 */
exports.apiUser = async (req, res, next) => {
  try {
    const response = await axios.get(apiRoutes.APIUSER_ROUTE, {
      auth: {username: config.apiUsername, password: config.apiPassword},
      timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
    });
    return res.send(response.data.user);
  } catch (error) {
    if (!error.response) {
      return next(boom.failedDependency(`Unable to connect to the API, error: ${error.message}`));
    } else if (error.response.data.webApplicationException) {
      return next(boom.badRequest(error.response.data.webApplicationException.message));
    } else {
      return next(boom.badImplementation(error.message));
    }
  }
};
