const axios = require('axios');
const apiRoutes = require('../constants/apiRoutes');
const boom = require('boom');


/**
 * Used to retrieve the list of valid countries (ISO codes and display names).
 * 
 * @param req {Object} - The request.
 * @param res {Object} - The response. 
 * @param next {Function} - Express next middleware function
 
 * @returns the list of country data type values
 */
exports.countries = async (req, res, next) => {
  try {
    const response = await axios.get(apiRoutes.PUBLIC_ROUTE + 'countries', {
      timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
    });
    return res.send(response.data.List);
  } catch (error) {
    if (!error.response) {
      return next(boom.failedDependency(`Unable to connect to the API, error: ${error.message}`));
    } else {
      return next(boom.badRequest(error.message));
    }
  }
};

/**
 *  Used to retrieve the list of valid languages (ISO codes and display names).
 * 
 * @param req {Object} - The request.
 * @param res {Object} - The response. 
 * @param next {Function} - Express next middleware function
 
 *  @returns the list of language data type values
 */
exports.languages = async (req, res, next) => {
  try {
    const response = await axios.get(apiRoutes.PUBLIC_ROUTE + 'languages', {
      timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
    });
    return res.send(response.data.List);
  } catch (error) {
    if (!error.response) {
      return next(boom.failedDependency(`Unable to connect to the API, error: ${error.message}`));
    } else {
      return next(boom.badRequest(error.message));
    }
  }
};

/**
 *  Used to check the health of the server.
 * 
 * @param req {Object} - The request.
 * @param res {Object} - The response. 
 * @param next {Function} - Express next middleware function
 
 *  @returns the current date of the server
 */
exports.time = async (req, res, next) => {
  return res.send(new Date());
};


