const joi = require('joi');

/**
 * This file defines the validation rules for all the values received from the client in a request call.
 */

module.exports.products = {
  params: {
    /* id of the operator */
    operatorId: joi.number().integer().min(1).required()
  }
}