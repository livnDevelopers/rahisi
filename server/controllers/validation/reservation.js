const joi = require('joi');

/**
 * This file defines the validation rules for all the values received from the client in a request call.
 */

module.exports.retrieve = {
  params: {
    /* id of the reservation */
    reservationId: joi.number().integer().min(1).required()
  }
};

module.exports.tickets = {
  params: {
    /* id of the reservation */
    reservationId: joi.number().integer().min(1).required()
  }
};

module.exports.search = {
  query: {
    /* Pax last name or email address (min 3 characters) */
    searchValue: joi.string().min(3).required(),
    startDate: joi.string(),
    endDate: joi.string()
  }
};