const config = require('../config/config');
const axios = require('axios');
const apiRoutes = require('../constants/apiRoutes');
const boom = require('boom');
const {logger} = require('../config/logger');


/**
 * Retrieves a specific reservation.
 *
 * @param req {Object} - The request.
 * @param res {Object} - The response. 
 * @param req.params.reservationId {integer}.
 * @param next {Function} - Express next middleware function
 * 
 * @returns The reservation object
 */
exports.retrieve = async (req, res, next) => {

  try {
    let response = await axios.get(apiRoutes.RESERVATIONS_ROUTE + req.params.reservationId, {
      auth: {username: config.apiUsername, password: config.apiPassword},
      timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
    });
    logger.info({message: 'Retrieved reservation', contents: response.data.reservation, ip: req.ip});
    return res.send(response.data.reservation);
  } catch (error) {
    if (!error.response) {
      return next(boom.failedDependency(`Unable to connect to the API, error: ${error.message}`));
    } else if (error.response.data.webApplicationException) {
      return next(boom.badRequest(error.response.data.webApplicationException.message));
    } else {
      return next(boom.badImplementation(error.message));
    }
  }
};


/**
 * Retrieves all reservations belonging to your API user's outlet group, by pax name/email, ignoring case and including partial matches.
 *
 * @param req {Object} - The request.
 * @param res {Object} - The response. 
 * @param req.query.searchValue {String} - Pax last name or email address.
 * @param next {Function} - Express next middleware function
 * 
 * @returns A list of reservations
 */
exports.search = async (req, res, next) => {

  try {
    let response = await axios.get(apiRoutes.RESERVATIONS_ROUTE + 'search', {
      params: {
        q: req.query.searchValue,
        startDate: req.query.startDate,
        endDate: req.query.endDate,
      },
      auth: {username: config.apiUsername, password: config.apiPassword},
      timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
    });
    logger.info({message: 'Searched for reservation', valueSearched: req.query.searchValue, ip: req.ip});
    // Only returning the first 12 reservations found
    return res.send(response.data.List.slice(0, 10));
  } catch (error) {
    if (!error.response) {
      return next(boom.failedDependency(`Unable to connect to the API, error: ${error.message}`));
    } else if (error.response.data.webApplicationException) {
      return next(boom.badRequest(error.response.data.webApplicationException.message));
    } else {
      return next(boom.badImplementation(error.message));
    }
  }
};

/**
 * Retrieves a collated PDF with all tickets for the specified reservation.
 *
 * @param req {Object} - The request.
 * @param res {Object} - The response. 
 * @param req.params.reservationId {integer}
 * @param next {Function} - Express next middleware function
 * 
 * @returns A collated PDF with all tickets for the specified reservation
 */
exports.tickets = async (req, res, next) => {

  try {
    let response = await axios.get(apiRoutes.RESERVATIONS_ROUTE + `${req.params.reservationId}/tickets`, {
      auth: {username: config.apiUsername, password: config.apiPassword},
      responseType: 'arraybuffer',
      timeout: apiRoutes.TIMEOUT_120 // After two minutes cancel the request if no response has been received.
    });
    res.writeHead(200, {
      'Content-Type': 'application/pdf',
      'Content-disposition': 'attachment'
    });
    logger.info({message: 'Retrieved tickets for reservation', reservationId: req.params.reservationId, ip: req.ip});
    return res.end(response.data, 'binary');
  } catch (error) {
    if (!error.response) {
      return next(boom.failedDependency(`Unable to connect to the API, error: ${error.message}`));
    } else if (error.response.data.webApplicationException) {
      return next(boom.badRequest(error.response.data.webApplicationException.message));
    } else {
      return next(boom.badImplementation(error.message));
    }
  }
};
